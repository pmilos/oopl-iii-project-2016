package pl.edu.pg.ftims.videoStore;

/**
 * Created by Paweł on 2017-01-21.
 */
//Interface nr.2
public interface ReturnMovie {
    public void returnMovie(Movie movie) throws ReturnError;
}
