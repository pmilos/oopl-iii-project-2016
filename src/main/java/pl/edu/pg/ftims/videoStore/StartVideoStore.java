package pl.edu.pg.ftims.videoStore;

/**
 * Created by Paweł on 2017-01-21.
 */
public class StartVideoStore {

    public static void main(String[] args) throws ReturnError {

        VideoStore videoStore = new VideoStore();

        Human human1 = new LoyaltyCard("Jeff","Smith",videoStore);
        Human human2 = new GoldCardClient("Mike","Kowalsky",videoStore);

        Movie M1 = new Movie("Law Abiding Citizen","F.Gary Grey","2009");
        Movie M2 = new Movie("I Am Legend","Francis Lawrence","2007");
        Movie M3 = new Movie("The Godfather","Francis Ford Coppola","1972");
        Movie M4 = new Movie("The Godfather II","Francis Ford Coppola","1974");
        Movie M5 = new Movie("Fight Club","David Fincher","1999");
        Movie M6 = new Movie("Interstellar","Christoper Nolan","2014");
        Movie M7 = new Movie("Intouchables","Oliver Nakache & Eric Toledano","2011");
        Movie M8 = new Movie("Whiplash","Damien Chazelle","2014");
        Movie M9 = new Movie("Limitless","Neil Burger","2011");
        Movie M10 = new Movie("Big Hero 6","Don Hall & Chris Williams","2014");

        //Adding movies to videoStore
        M1.addMovieToVideoStore(videoStore);
        M2.addMovieToVideoStore(videoStore);
        M3.addMovieToVideoStore(videoStore);
        M4.addMovieToVideoStore(videoStore);
        M5.addMovieToVideoStore(videoStore);
        M6.addMovieToVideoStore(videoStore);
        M7.addMovieToVideoStore(videoStore);
        M8.addMovieToVideoStore(videoStore);
        M9.addMovieToVideoStore(videoStore);
        M10.addMovieToVideoStore(videoStore);

        //Printing list of movies and list of clients
        videoStore.printMovies();
        videoStore.printClients();

        //Client number 1 borrowing M1 & M2
        human1.borrowMovie(M1);
        human1.borrowMovie(M2);
        //Printing map to check status of borrowed films by Client number 1
        human1.mapInformation();
        //Client number 1 is trying to borrow 3rd movie
        human1.borrowMovie(M3);
        //Printing map of Client number 1 to check that he could not borrow 3rd movie
        human1.mapInformation();

        //Client number 2 is trying to borrow movie M1, which is already borrowed
        human2.borrowMovie(M1);
        human2.borrowMovie(M10);
        human2.borrowMovie(M8);
        //Printing map of Client number 2 to check status of borrowed films
        human2.mapInformation();

        //Client number 1 is returning movie M1
        human1.returnMovie(M1);

        //Command below is to show the exception is working (uncomment for test)
        //human1.returnMovie(M1);

        human1.mapInformation();

        //Client number 2 is trying to borrow movie M1 again
        human2.borrowMovie(M1);
        human2.mapInformation();

        videoStore.printMovies();
    }
}
