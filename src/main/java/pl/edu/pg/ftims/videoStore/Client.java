package pl.edu.pg.ftims.videoStore;

/**
 * Created by Paweł on 2017-01-21.
 */
public abstract class Client extends Human {

    public Client(String name ,String surname ,VideoStore videoStore){
        this.name = name;
        this.surname = surname;
        videoStore.addClient(this);
    }


    public void informationAboutClient(){
        System.out.println(this.name+" "+this.surname);
    }
}
