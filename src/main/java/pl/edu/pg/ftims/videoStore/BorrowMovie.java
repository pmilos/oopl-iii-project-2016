package pl.edu.pg.ftims.videoStore;

/**
 * Created by Paweł on 2017-01-21.
 */
//Interface nr.1
public interface BorrowMovie {
    public void borrowMovie(Movie movie);
}
