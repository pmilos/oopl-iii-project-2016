package pl.edu.pg.ftims.videoStore;

/**
 * Created by Paweł on 2017-01-22.
 */
//Exception is used in classes 'LoyaltyCard' & 'GoldCardClient'
public class ReturnError extends Exception {
    public ReturnError(){
        System.out.println("You can not return this Movie, you do not have it.");
    }
}
