package pl.edu.pg.ftims.videoStore;

/**
 * Created by Paweł on 2017-01-21.
 */
public class Movie {

    private String title;
    private String director;
    private String production_date;
    private boolean borrowed;

    public Movie(String title, String director, String production_date){
        this.title = title;
        this.director = director;
        this.production_date = production_date;
        this.borrowed = false;
    }

    public void informationAboutMovie(){
        System.out.println("'"+this.title+"'"+" "+this.director+" ("+this.production_date+")"+">(Borrowed: "+getBorrowed()+")");
    }

    public Boolean getBorrowed(){
        return borrowed;
    }
    public void setBorrowed(Boolean borrowed1) {
        borrowed = borrowed1;
    }

    public void addMovieToVideoStore(VideoStore videoStore){
        videoStore.addMovies(this);
    }

    public String toString() {
        return this.title+" "+this.director+"("+this.production_date+")";
    }
}
