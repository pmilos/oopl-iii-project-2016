package pl.edu.pg.ftims.videoStore;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-01-21.
 */
public class VideoStore {

    private ArrayList<Movie> listOfMovies = new ArrayList<Movie>();
    private ArrayList<Client> listOfClients = new ArrayList<Client>();

    public void addMovies(Movie movie){
        this.listOfMovies.add(movie);
    }

    public void addClient(Client client){
        this.listOfClients.add(client);
    }

    public void printClients(){
        for (Client client : listOfClients){
            client.informationAboutClient();
        }
    }

    public void printMovies(){
        for (Movie movie : listOfMovies){
            movie.informationAboutMovie();
        }
        System.out.println();
    }
}
