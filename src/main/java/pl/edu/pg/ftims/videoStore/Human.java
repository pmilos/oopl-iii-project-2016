package pl.edu.pg.ftims.videoStore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 * Created by Paweł on 2017-01-21.
 */
public abstract class Human implements BorrowMovie,ReturnMovie {

    protected String name;
    protected String surname;
    //HashMap
    protected HashMap<Movie,Human> mapOfHumanAndBorrowedMovies = new HashMap<Movie,Human>();

    //List
    protected ArrayList<Movie> BorrowedMovies = new ArrayList<Movie>();

    public String toString(){
        return (this.name+" "+ this.surname);
    }
    public void mapInformation(){
        for (Entry<Movie,Human>entry : mapOfHumanAndBorrowedMovies.entrySet()){
            Movie key = entry.getKey();
            Human value = entry.getValue();
            System.out.println(value.toString()+">(Borrowed: "+key.toString()+")");
        }
    }
}
