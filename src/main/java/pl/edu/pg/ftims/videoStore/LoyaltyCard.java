package pl.edu.pg.ftims.videoStore;

/**
 * Created by Paweł on 2017-01-21.
 */
public class LoyaltyCard extends Client{

    private final static int maxBorrowedMovies = 2;

    public LoyaltyCard(String name ,String surname ,VideoStore videoStore){

        super(name,surname,videoStore);
    }

    public void borrowMovie(Movie movie){
        if (!movie.getBorrowed()&&BorrowedMovies.size()<LoyaltyCard.maxBorrowedMovies){
            BorrowedMovies.add(movie);
            mapOfHumanAndBorrowedMovies.put(movie,this);
            movie.setBorrowed(true);
        }
        else if (BorrowedMovies.size()>=LoyaltyCard.maxBorrowedMovies){
            System.out.println(this.name+" you can not borrow more movies at the moment, you need to return some borrowed movie first.");
        }
        else {
            System.out.println(this.name+" you can not borrow this movie at the moment, it's actually borrowed.");
        }
    }


    public void returnMovie(Movie movie) throws ReturnError {
        for (int i = 0; i < BorrowedMovies.size() ; i++) {
            if (BorrowedMovies.contains(movie)){
                System.out.println("Return of your movie has been registered.");
                mapOfHumanAndBorrowedMovies.remove(movie,this);
                movie.setBorrowed(false);
                BorrowedMovies.remove(movie);
                return;
            }
            else{
                throw new ReturnError();
            }
        }
    }

}
